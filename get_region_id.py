from area_list import area_list

def find_region_code(area):
    """
    查询行政区划代码
    :param area: "河南省驻马店市驿城区驿城社区6号楼16号"
    :return:
    success： ('河南省', '驻马店市', '驿城区', '411702')
    error： ('', '', '', 0)
    """
    # 验证第一层
    area_list_two, area_list_three = list(), list()
    one_name, two_name, three_name, area_id = "", "", "", 0
    for one in area_list:
        one_name = str(one.get("name"))
        if one_name in area:
            print(one.get("code"))
            area_list_two = one.get("areaList")
            break
        else:
            one_name = ""

    # 验证第三层(身份证地级县没有显示市名，因此要将第三层的全部过滤一遍)，
    for two in area_list_two:
        area_list_three = two.get("areaList")
        # print(area_list_three)
        two_name = two.get("name")
        for three in area_list_three:
            three_name = str(three.get("name"))
            # 如果是县级没有重名只需要验证县名
            if "县" in three_name:
                if three_name in area:
                    print("three", three)
                    area_id = three.get("code")
                    print(area_id)
                    return one_name, two_name, three_name, area_id
            else:
                # 如果是县级市地名，需要验证上级市名
                if (three_name in area) and ((two_name in area) or (one_name in area)):
                    print("three", three)
                    area_id = three.get("code")
                    print(area_id)
                    return one_name, two_name, three_name, area_id
    return one_name, "", "", 0


if __name__ == '__main__':
    s_qu = "河南省驻马店市驿城区驿城"
    s_xain = "河南省辉县市城关镇四新街202号"
    s_qu1 = "河南省漯河市源汇区大留镇"

    res = find_region_code(s_qu1)
    print(res)
